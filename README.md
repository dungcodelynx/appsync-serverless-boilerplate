# appsync-example

This project demonstrates basically how to deploy an AWS-appsync with dynamoDB and AWS-lambda as data sources using serverless framework.

### This example needs:
- NodeJS v8
- AWS account
- serverless framework (https://serverless.com/)
- serverless-appsync-plugin (https://github.com/sid88in/serverless-appsync-plugin)

# Step 1: Setup Serverless framework
- First is to install Serverless framework (details in this guide: https://serverless.com/framework/docs/providers/aws/guide/installation/): 
``` 
npm install -g serverless 
```

- Secondly, setting up credentials to connect with Amazon services. Please follow this guide: https://serverless.com/framework/docs/providers/aws/guide/credentials/

# Step 2: Define schema for GraphQL
After cloned and installed package, now we define the schema.
Schema in this example had been defined in [schema.graphql](./schema.graphql) 
and it looks like this:
```
type Post{
    id: ID!
    description: String!
}

type Mutation {
    createPost(description: String!): Post
    deletePost(id: ID!)
}

type Query {
    getAllPosts: [Post]
    getPost(id: ID!): Post
    getResultLambda: String
}

type schema {
    query: Query
    mutation: Mutation
}
```
`Please note that annotations in Graphcool can not be used (it will raise error)`
# Step 3: Define mapping templates
Usually, the communication is through parameters or operations that are unique to the data source. 
For an AWS Lambda resolver, you need to specify the payload. For an Amazon DynamoDB resolver, you need to specify a key.
Mapping templates are a way of indicating to AWS AppSync how to translate an incoming GraphQL request into instructions for your backend data source, 
and how to translate the response from that data source back into a GraphQL response. They are written in Apache Velocity Template Language (VTL).
(More details in [Resolver Mapping Template Overview](https://docs.aws.amazon.com/appsync/latest/devguide/resolver-mapping-template-reference-overview.html))

Our mapping templates are in folder [mapping-templates](./mapping-templates)

# Step 4: Config ```serverless.yml```
[serverless.yml](./serverless.yml) file is what we use to deploy all the things into Amazon services using Serverless framework.
Lets dig into it:
- ### Config provider
```
service: appsync-example # NOTE: update this with your service name

provider:
  name: aws  --> Define provider is Amazon 
  runtime: nodejs8.10  --> with nodejs (v8) runtime environment
  region: us-east-1  --> default region for services

plugins:
  - serverless-appsync-plugin  --> plugin to use
```
Check out other regions [here](https://docs.aws.amazon.com/general/latest/gr/rande.html).
- ### Config Appsync
```
custom:
  appSync:
    name: appsync-example  --> Name of you appsync api
    authenticationType: API_KEY # API_KEY or AWS_IAM or AMAZON_COGNITO_USER_POOLS or OPENID_CONNECT
    mappingTemplatesLocation: ./mapping-templates  --> path to mapping templates folder
    schema: ./schema.graphql  --> path to schema file
    mappingTemplates:
      - (define which data source to use, which request & response template to use for each Query/Mutation/Subscription in the schema)
    serviceRole: "AppSyncServiceRole"
    dataSources:
      - (define the data sources that is used in mappingTemplates section above)
          
```
That is basically what we need to config Appsync. However, we still have `mappingTemplates` and `dataSources` section to be filled.
- ### Config Appsync with AWS Lambda as data source

This part will show how to fill in `mappingTemplates` and `dataSources` section to deploy Appsync with a AWS Lambda function as data source. In other words,
this will create an appsync api and a Lambda function.
```
functions:
  hello:  --> define a Lambda function
    handler: handler.hello
    
custom:
  appSync:
    name: appsync-example
    authenticationType: API_KEY # API_KEY or AWS_IAM or AMAZON_COGNITO_USER_POOLS or OPENID_CONNECT
    mappingTemplatesLocation: ./mapping-templates
    mappingTemplates:
      - dataSource: Lambda_Hello  --> name of data source to be used
        type: Query
        field: getResultLambda
        request: lambda-request.vtl  --> request mapping template for this query
        response: common-response.vtl  --> response mapping template for this query
    schema: schema.graphql
    serviceRole: "AppSyncServiceRole"
    dataSources:
      - type: AWS_LAMBDA
        name: Lambda_Hello
        description: 'Lambda DataSource for appsync-example'
        config:
          lambdaFunctionArn: { Fn::GetAtt: [HelloLambdaFunction, Arn] }
```
You might wonder what this line `lambdaFunctionArn: { Fn::GetAtt: [HelloLambdaFunction, Arn] }` means. Because this Lambda function will be created with
Appsync so we want to reference its Arn (Amazon resource name) so that Appsync knows exactly which Lambda function we use as data source.
- ### Config Appsync with DynamoDB as data source

To create DynamoDB table or use it as data source, we use `resources`

```
custom:
  appSync:
    name: appsync-example
    authenticationType: API_KEY # API_KEY or AWS_IAM or AMAZON_COGNITO_USER_POOLS or OPENID_CONNECT
    mappingTemplatesLocation: ./mapping-templates
    mappingTemplates:
      - dataSource: Dynamodb_Post
        type: Mutation
        field: createPost
        request: put-item-request.vtl
        response: common-response.vtl
    schema: ./schema.graphql
    serviceRole: "AppSyncServiceRole"
    dataSources:
      - type: AMAZON_DYNAMODB
        name: Dynamodb_Post
        description: 'Post table in DynamoDB for appsync-example'
        config:
          tableName: { Ref: PostTable }  --> reference to Arn of table
resources:
  Resources:
    PostTable:
      Type: 'AWS::DynamoDB::Table'
      Properties:
        KeySchema:
          -
            AttributeName: id
            KeyType: HASH
        AttributeDefinitions:
          -
            AttributeName: id
            AttributeType: S
        ProvisionedThroughput:
          ReadCapacityUnits: 5
          WriteCapacityUnits: 5
        TableName: 'PostTable'
```
# Step 5: Deploying
Now try deploying this Appsync using Serverless framework
```
sls deploy
```
Having errors? Check this [Encounter errors](#encounter-errors) section.

After deployed, go to AWS Appsync dashboard and make some query with your created Appsync api using its playground. For example,
```
query {
  getResultLambda
}
```
The result is
```
{
  "data": {
    "getResultLambda": "{statusCode=200, body={\"message\":\"Hello from the other sideeeee\"}}"
  }
}
```
Don't know how to do it? Read this guide: https://docs.aws.amazon.com/en_us/appsync/latest/devguide/quickstart.html
# Step 6: Running deployed Appsync with React Apollo front-end
React Apollo app used in this example was cloned from this repo 
[React & Apollo Quickstart](https://github.com/graphcool-examples/react-graphql/tree/master/quickstart-with-apollo)
(with some modifications to suit with this example project).

To have client communicated with Appsync, we need to provide endpoint as well as access key. Go to your Appsync api, choose Settings and there
they are:
- API URL: graphQL endpoint
- API KEY: since we used API key as Authorization type so this is what we need

Once we have endpoint and key, open [index.js](./src/index.js) and edit as follow:
```
const httpLink = new HttpLink({
	uri: '<your_endpoint>',
	headers: {
		'x-api-key': '<your_api_key>',
		'content-type': 'application/json'
	}
});
```

Save it, `npm start` and enjoy!

# Encounter errors
1. Missing package "aws-sdk"
> Fix: install it
2. Do not have permission/role...
> Fix: in this example project, maybe you should use the AWS user with administrator access (for simplification)
